TARGET=root
PATH_IMG=$(wildcard img/*)
TARGET_TEX=${TARGET}.tex
TARGET_PDF=${TARGET}.pdf
TARGET_BIB=${TARGET}.bib
DATE=`date +%Y%m%d%H%M%S `
default: clean ${TARGET_PDF}

all: ${TARGET_PDF}

${TARGET_PDF}: ${TARGET_BIB} ${PATH_IMG} ${TARGET_TEX} 
	latexmk -pdf ${TARGET}

clean:
	@rm -f ${TARGET_PDF}
	@rm -f ${TARGET}.aux*
	@rm -f ${TARGET}.bbl*
	@rm -f ${TARGET}.blg*
	@rm -f ${TARGET}.dvi*
	@rm -f ${TARGET}.fls*
	@rm -f ${TARGET}.log*
	@rm -f ${TARGET}.lof*
	@rm -f ${TARGET}.lot*
	@rm -f ${TARGET}.nav*
	@rm -f ${TARGET}.out*
	@rm -f ${TARGET}.snm*
	@rm -f ${TARGET}.toc*
	@rm -f ${TARGET}.ist*
	@rm -f ${TARGET}.glo*
	@rm -f ${TARGET}.fdb_latexmk*
	@rm -f *~



view: ${TARGET_PDF}
	@evince $?
